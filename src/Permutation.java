package com.company;

import java.util.Arrays;

import static java.lang.Math.ceil;
import static java.lang.Math.floor;

//reference https://crypto.interactive-maths.com/permutation-cipher.html#act

public class Permutation {
    private int[] key;
    public int keyLength;

    private int[] inverseKey;

    public Permutation(String key) {
        char[] sortedKeyArr = key.toCharArray();
        Arrays.sort(sortedKeyArr);
        char[] unsortedKeyArr = key.toCharArray();

        keyLength = sortedKeyArr.length;

        int[] numKey = new int[keyLength];
        for(int i = 0; i < keyLength; i++) {
            for(int j = 0; j < keyLength; j++) {
                if(unsortedKeyArr[i] == sortedKeyArr[j]) {
                    //check previous characters to see if they have already occurred to assign them a larger value
                    int tj = j;
                    for(int k = 0; k < i; k++) {
                        if(unsortedKeyArr[k] == unsortedKeyArr[i]) {
                            tj++;
                        }
                    }
                    numKey[i] = tj;
                    break;
                }
            }
        }
        this.key = numKey;
        int[] dnumKey = new int[keyLength];
        for(int i = 0; i < keyLength; i++) {
            int tj = 0;
            for(int k = 0; k < i; k++) {
                if(sortedKeyArr[i] == sortedKeyArr[k]) {
                    tj++;
                }
            }
            for(int j = 0; j < keyLength; j++) {
                if(sortedKeyArr[i] == unsortedKeyArr[j]) {
                    if(tj > 0) {
                        tj--;
                    } else {
                        dnumKey[i] = j;
                        break;
                    }
                }
            }
        }
        this.inverseKey = dnumKey;
        //System.out.println("key: " + Arrays.toString(numKey));
        //System.out.println("inverse key: " + Arrays.toString(dnumKey));
    }

    public String getKey() {
        return Arrays.toString(this.key);
    }

    private String crypt(int[] key, String message) {
        char[] messageArr = message.toCharArray();

        char[] encryptedArr = new char[(int) (ceil(messageArr.length/(double)keyLength) * keyLength)];
        Arrays.fill(encryptedArr, '?');
        //fill array with ? to then delete those characters to remove padding

        for(int i = 0; i < encryptedArr.length; i++) {
            if(i >= messageArr.length) {
                encryptedArr[(key[i%keyLength] + (int)(floor(i/keyLength) * keyLength))] = '?';
            } else {
                encryptedArr[(key[i % keyLength] + (int) (floor(i / keyLength) * keyLength))] = messageArr[i];
            }
        }

        //return new String(encryptedArr).replace("?", "");
        return new String(encryptedArr);
    }

    public String encrypt(String message) {
        return crypt(this.key, message);
    }

    public String decrypt(String message) {
        return crypt(this.inverseKey, message).replace("?", "");
        //return crypt(this.inverseKey, message);
    }

}
