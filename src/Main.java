package com.company;

public class Main {
    public static void main(String[] args) throws Exception{
        Substitution.sampleCase();
        Substitution.userCase();

        System.out.println("\n");
        String k = "fkjkfkfj";
        Permutation p = new Permutation(k);
        String s = "testing this longer sentence hello hello world. this is an example of sentence that i will hopefully decipher, it will be cool.";
        String c = p.encrypt(s);
        System.out.println("message: " + s);
        System.out.println("key = " + k);
        System.out.println("encrypted: " + c);
        System.out.println("decrypted: " + p.decrypt(c) + "\n");

        PermutationAttack pa = new PermutationAttack(s, c);

        Permutation patkd = new Permutation(pa.key);
        System.out.println("testing key '" + pa.key + "': " + patkd.decrypt(c));
    }
}
