package com.company;

import java.util.Scanner;

//reference https://crypto.interactive-maths.com/mixed-alphabet-cipher.html

public class Substitution {
    public static final String baseKey = "abcdefghijklmnopqrstuvwxyz ,.";

    public char[] key;

    public Substitution(String k) throws Exception{
        key = k.toCharArray();
        if(key.length != 29) {
            throw new Exception("cipher key not in Z29");
        }
    }
    public Substitution(char[] k) throws Exception{
        key = k;
        if(key.length != 29) {
            throw new Exception("cipher key not in Z29");
        }
    }

    public String getKey() {
        return new String(key);
    }

    //function encrypts a message using substitution with a given key
    public String encryptMessage(String text) {
        StringBuilder encryptedText = new StringBuilder();

        //encrypt each char by using the characters integer value to index the key string
        for(int i = 0; i < text.length(); i++) {
            int c = (int)text.charAt(i);
            if(c <= 'z' && c >= 'a') {
                int x = c - 'a';
                encryptedText.append(key[x]);
            } else if (c == ' '){
                encryptedText.append(key[26]);
            } else if (c == ',') {
                encryptedText.append(key[27]);
            } else { // c == '.'
                encryptedText.append(key[28]);
            }
        }
        return encryptedText.toString();
    }

    //function decrypts a message using substitution with a given key
    public String decryptMessage(String text) {
        //create an inverse key to decrypt the message
        char[] inverseKey = new char[29];

        for(int i = 0; i < 29; i++) {
            int c = (int) key[i];
            char b = baseKey.charAt(i);
            if (c <= 'z' && c >= 'a') {
                inverseKey[c - 'a'] = b;
            } else if (c == ' ') {
                inverseKey[26] = b;
            } else if (c == ',') {
                inverseKey[27] = b;
            } else { // c == '.'
                inverseKey[28] = b;
            }
        }

        //use the encryption function to encrypt the message with the inverse key resulting in the message being unencrypted
        try {
            return new Substitution(inverseKey).encryptMessage(text);
        } catch (Exception e) {
            e.printStackTrace();
            return text;
        }
    }

    //function that uses sample case
    public static void sampleCase() {
        //Create sample key, message, encrypted message, and decrypted message
        Substitution sample;
        try {
            sample = new Substitution("zebrascdfghijklmnopqtuvwxy .,");
        String sampleMessage = "flee at once, we are discovered.";
            String sampleEncrypted = sample.encryptMessage(sampleMessage);
            String sampleDecrypted = sample.decryptMessage(sampleEncrypted);

            //print out debug info on sample variables
            System.out.println("Using sample key and sample message...\n");
            System.out.println("base:\t" + baseKey
                           + "\nkey: \t" + sample.getKey());
            System.out.println("message:\t" + sampleMessage);
            System.out.println("encrypted:\t" + sampleEncrypted);
            System.out.println("decrypted:\t" + sampleDecrypted + "\n");
        } catch (Exception ex) {
            System.err.println(ex);
        }
    }

    //function that uses user input
    public static void userCase() {
        //user inputted
        Scanner sc = new Scanner(System.in);

        //User's key
        System.out.println("\t\t\t\t\t\t\t\t\t\t   " + baseKey);
        System.out.print("Enter a key (using all characters in Z29): ");
        String userKey = sc.nextLine();

        //User's message
        System.out.print("Enter a message to encrypt: ");
        String userMessage = sc.nextLine();

        try {
            //User encryption
            Substitution user = new Substitution(userKey);
            String userEncrypted = user.encryptMessage(userMessage);
            String userDecrypted = user.decryptMessage(userEncrypted);

            //print user results
            System.out.println("encrypted:\t\t\t\t\t" + userEncrypted);
            System.out.println("decrypted:\t\t\t\t\t" + userDecrypted);
        } catch(Exception ex) {
            System.err.println("Error user inputted invalid value\n" + ex);
        }
    }
}
