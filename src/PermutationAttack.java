package com.company;

import java.util.ArrayList;
import java.util.Arrays;

import static java.lang.Math.*;

public class PermutationAttack {

    public String key;

    PermutationAttack(String message, String cipherText) {
        int l = cipherText.length();
        int m = message.length();

        //possible key length values that create amount of padding (uses only extra length on ciphertext)
        int[] a = getDivisors(l, l-m);
        System.out.println("key length must be in array: " + Arrays.toString(a));

        //add extra padding back accordingly
        if(m < l) {
            for(int i = 0; i < l-m; i++) {
                message += "_";
            }
        }

        //get a string of possible relative moves
        char[] c = cipherText.toCharArray();
        char[] p = message.toCharArray();
        int[] relativeSwap = new int[l];
        boolean[] hasSwapped = new boolean[l];
        Arrays.fill(hasSwapped, false);
        for(int i = 0; i < l; i++) {
            for(int j = 0; j < l; j++) {
                if(c[i] == p[j] && !hasSwapped[j]) {
                    relativeSwap[i] = j-i;
                    hasSwapped[j] = true;
                    //System.out.println(i + "\t" + relativeSwap[i] + "\t" + c[i] + "\t" + p[i]);
                    break;
                }
            }
        }

        //figure out most probably key length
        int pKeyLength = 0;
        int repeatNum = 0;
        int delta = 0;
        for(int i = 0; i < (l+1)/2; i++) {
            delta += relativeSwap[i];
            if(delta == 0) {
                System.out.print("testing length " + (i+1) + " ... ");
                for(int k = 2; true; k++) {
                    for (int j = i + 1; j < (k * (i + 1)) && j < l; j++) {
                        delta += relativeSwap[j];
                    }
                    if(delta != 0) {
                        if(k == 2) {
                            System.out.println("*Failed*");
                        } else {
                            System.out.println("Repeated " + k + " times");
                            if(Math.max(repeatNum, k) == k) {
                                pKeyLength = i+1;
                                repeatNum = k;
                            }
                        }
                        delta = 0;
                        break;
                    }
                }
            }
        }

        System.out.println("\nThe key is most probably " + pKeyLength + " characters long");
        int probRecur = 0;
        int[] probKey = new int[pKeyLength];
        int[][] allkeys = new int[repeatNum][pKeyLength];
        int tempCounter = 0;
        //divide array into chunks
        for(int i=0;i<relativeSwap.length;i+=pKeyLength){
            int[] ar = Arrays.copyOfRange(relativeSwap, i, Math.min(relativeSwap.length,i+pKeyLength));
            //find all 'keys' that have a relative sum of zero and are unique
            if(relativelyZero(ar)) {
                int tempC = 0;
                for(int j = 0; j < repeatNum; j++) {
                    if(!compareArrays(ar, allkeys[j])) {
                        tempC++;
                    }
                }
                if(tempC >= repeatNum) { //ar is unique
                    allkeys[tempCounter] = ar;
                    tempCounter++;
                } else { //ar has already occured
                    //find the most reaccuring array to assume is the most probably key
                    if (max(probRecur, repeatNum - tempC) != probRecur) {
                        probKey = ar;
                        probRecur = repeatNum - tempC;
                    }
                }
            }
        }

        //clean key arrays
        probKey = cleanArray(probKey);

        //add unique keys to arraylist
        ArrayList<int[]> keys = new ArrayList<int[]>();
        for(int i = 0; i < tempCounter; i++) {
            keys.add(cleanArray(allkeys[i]));
        }
        //print out all keys
        System.out.println("\nAll keys:");
        for(int i = 0; i < keys.size(); i++) {
            System.out.println(Arrays.toString(keys.get(i)));
        }
        System.out.println("Most Probable Key: " + Arrays.toString(probKey));

        this.key = toStringKey(probKey);

        System.out.println("As characters: " + toStringKey(probKey));
    }

    //convert key to characters
    private String toStringKey(int[] a) {
        String s = "";
        for(int i = 0; i < a.length; i++) {
            s += (char)(a[i] + 'A');
        }
        return s;
    }

    //see if relative difference between all elements is still zero (used for discounting padding errors)
    private boolean relativelyZero(int[] a) {
        int delta = 0;
        for (int j : a) {
            delta += j;
        }
        return delta == 0;
    }

    //assuming arrays are of equal length, returns if all elements are equal or not
    private boolean compareArrays(int[] a, int[] b) {
        for(int i = 0; i < a.length; i++) {
            if(a[i] != b[i]) {
                return false;
            }
        }
        return true;
    }

    //use the starting position now known from the length of the key to change
    //the relative swap index into the ordered key
    private int[] cleanArray(int[] a) {
        int[] b = a.clone();
        for(int i = 0; i < a.length; i++) {
            b[i+a[i]] = i+1;
        }
        return b;
    }

    // method to get divisors
    private int[] getDivisors(int d, int m)
    {
        ArrayList<Integer> nums = new ArrayList<Integer>();

        for (int i = m+1; i <= d; i++) {
            if (d % i == 0) {
                nums.add(i);
            }
        }
        Object[] ar = nums.toArray();
        int[] arr = new int[ar.length];
        for(int i = 0; i < arr.length; i++) {
            arr[i] = (int)ar[i];
        }
        return arr;
    }
}
