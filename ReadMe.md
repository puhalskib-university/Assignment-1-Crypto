# ReadMe

Ben Puhalski - Substitution cipher, Permutation cipher, and known plaintext attack on a permutation cipher.

(2021W) COMP-4476-WDE Cryptography  Network Security

# Substitution Cipher

Reference - https://crypto.interactive-maths.com/mixed-alphabet-cipher.html

![OneOutA](https://gitlab.com/puhalskib-university/Assignment-1-Crypto/-/raw/main/fig/AssignmentOneOutA.png)

The above is the output from the substitution cipher program. The green text is user inputted by me in this case. It can be inputted by anyone else and will still function correctly. All that is done is that it swaps out all the characters in the message to the corresponding message chosen in the key.

#### Limitations

You have to type in every character for the key. Should have implemented an autofill function that added the extra characters in order.



The **Substitution** class has the following public methods and variables

### Constructors

| Substitution(String key) throws Exception | throws exception if key is not 29 characters |
| ----------------------------------------- | -------------------------------------------- |
| Substitution(char[] key) throws Exception | throws exception if key is not 29 characters |

### Public Methods

| String getKey()                    | returns the key : String                                     |
| ---------------------------------- | ------------------------------------------------------------ |
| String encryptMessage(String text) | Params: String for the message to be encrypted<br />Returns encrypted text : String; using the key |
| String decryptMessage(String text) | Params: String for the message to be decrypted<br />Returns unencrypted text : String; using the key |
| static void sampleCase()           | Static method used to showcase the cipher using a sample key and message |
| static void userCase()             | Static method used to showcase the cipher using user inputted key and message |

### Public Properties

| static final String baseKey | The base case key in Z29 in alphabetical order               |
| --------------------------- | ------------------------------------------------------------ |
| char[] key                  | The key used for this substitution cipher instance (assigned in constructor) |

# Permutation Cipher

Reference - https://crypto.interactive-maths.com/permutation-cipher.html#act

![permout](https://gitlab.com/puhalskib-university/Assignment-1-Crypto/-/raw/main/fig/Screenshot%202021-02-02%20210222-permout.png)

The above is the output from the permutation cipher. It shows a sample message and key used in the program. The below 2 lines are the output from the cipher which is the corresponding encrypted text and decrypted text.

#### Limitations

The cipher uses '?' as null values/padding. This is very insecure and would be the first thing I change if I redid the program to use a different padding system.



The **Permutation** class has the following public methods and variables

### Constructors

| Permutation(String key) | Sets up key used by cipher; Allows duplicate characters, Essencially allows all characters tested<br />Using the padding character '?' will break the decryption function |
| ----------------------- | ------------------------------------------------------------ |
|                         |                                                              |

### Public Methods

| String getKey()                | returns the key : String; inputted during construction       |
| ------------------------------ | ------------------------------------------------------------ |
| String encrypt(String message) | Params: The message : String; to be encrypted<br />returns the encryped text : String |
| String decrypt(String message) | Params: The message : String; to be decrypted<br />returns the unencryped text : String |

### Public Variables

| int keyLength | The cipher key's length |
| ------------- | ----------------------- |
|               |                         |

# Permutation Attack

Reference - https://crypto.interactive-maths.com/permutation-cipher.html#act

<img src="https://gitlab.com/puhalskib-university/Assignment-1-Crypto/-/raw/main/fig/Screenshot%202021-02-02%20210254.png" style="zoom: 80%;" />

![](https://gitlab.com/puhalskib-university/Assignment-1-Crypto/-/raw/main/fig/Screenshot%202021-02-02%20210303.png)

The above is the output during the attack. The attack worked even though the key strings are different. This particular attack was essentially instantaneous and used:

**Message: **testing this longer sentence hello hello world. this is an example of sentence that i will hopefully decipher, it will be cool.

**key: **fkjkfkfj

Even though there are duplicate characters in the key, it will still function normally as stated before. It assigns the duplicate keys the next possible number when ordering the key and converting it to numbers it will be:

**key:** 1,6,4,7,2,8,3,5

The first line uses the extra padding on the message vs the ciphertext to determine what numbers would have resulted in a padding of that value. This line isn't used in the program but is useful for testing.

The program takes advantage of the fact that the first permutation must always start at the first character and may not swap/sort the characters before the string starts. It finds the first instance of a possible swap is on every character in the message and assigns the relative swap amount. At every repetition of the key in the message the sum of those relative swapping positions should be zero. The program figures out every time the sum is zero and tests to see if the sum of zero repeats. If it does not repeat even once, the program prints "*failed*". If it repeats more than once it will show how many times it repeats throughout the message. It assumes that the repeat value that repeats the most time **is the length of the key**.

Since there are duplicate characters in the message however, some of the strings/blocks/chunks of relative swaps are in a different order or are different values. They still result in a sum of zero when added together however. So, the algorithm deals with this by counting all of the strings/blocks/chunks and seeing which one occurs the most times and assumes that that string is key. 



### Limitations

The work around of this is just to test the strings instead and to essentially use a function similar to the encryption function in the permutation program to test all these blocks to see if they would work with the string. However, I did not want to use the permutation cipher program in my permutation attack program in this example. There would need to be many duplicate characters and many coincidences for this to be a problem, especially if the message is sufficiently large relative to the key size. For example, the above algorithm had 16 repeated keys of length 8 and 6 unique keys. 



The **PermutationAttack** class has the following public methods and variables

### Constructors

| PermutationAttack(String message, String cipherText) | Calculates the most probable key according to the algorithm and shows other possible keys if the most probable one is not correct. |
| ---------------------------------------------------- | ------------------------------------------------------------ |
|                                                      |                                                              |

### Public Variables

| String key | The most probable key as determined by the algorithm |
| ---------- | ---------------------------------------------------- |
|            |                                                      |

There are more private methods used within the program.

## Tools used

- IntelliJ IDEA Community Edition 2020.2.1

